package com.charity.cms.service;

import com.charity.cms.model.Category;
import com.charity.cms.model.Vote;
import org.springframework.data.domain.Pageable;

public interface CategoryService {
    Iterable<Category> findAll();
    Category findById(Long id);
    void save(Category category);
    void remove(Long id);

    Iterable<Category> findAllByVote(Vote vote);
    long countVoteVyCategory(Category category);
    long countAll();
}
