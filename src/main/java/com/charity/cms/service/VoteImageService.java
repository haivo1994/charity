package com.charity.cms.service;

import com.charity.cms.model.Vote;
import com.charity.cms.model.VoteImage;

import java.util.List;

public interface VoteImageService {
    Iterable<VoteImage> findAll();
    Iterable<VoteImage> findAllByVote(Vote vote);
    void save(VoteImage voteImage);
    void remove(Long id);
    void removeAllByVote(Vote vote);
    List<VoteImage> getTopImage(int size);
}
