package com.charity.cms.service.impl;

import com.charity.cms.model.Account;
import com.charity.cms.model.Volunteer;
import com.charity.cms.model.Vote;
import com.charity.cms.repository.AccountRepository;
import com.charity.cms.repository.DonateRepository;
import com.charity.cms.repository.VolunteerRepository;
import com.charity.cms.repository.VoteRepository;
import com.charity.cms.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;

import java.util.Date;
import java.util.List;

public class AccountServiceImpl implements AccountService {
    @Autowired
    private AccountRepository accountRepository;

    @Override
    public Account findById(Long id) {
        return accountRepository.findById(id).orElse(null);
    }

    @Override
    public Account findByUsername(String username) {
        return accountRepository.findFirstByUsername(username);
    }

    @Override
    public void update(Account originAccount, Account newAccount) {
        originAccount.setPassword(newAccount.getPassword());
        originAccount.setUpdated_at(new Date());
        originAccount.setUpdated_by(newAccount.getUsername());
        accountRepository.save(originAccount);
    }

    @Autowired
    private VolunteerRepository volunteerRepository;
    @Override
    public String create(String userName,String password,String yourName,String yourEmail,String yourPhone) {
        Account checkAccount = accountRepository.findFirstByUsername(userName);
        Volunteer volunteer;
        if(checkAccount!=null){
            return "Tên Tài Khoản Đã Tồn Tại ";
        }
        else{
            if (yourPhone!=null &&
                    !yourPhone.equals("")){
                volunteer = volunteerRepository.
                        findFirstByPhoneOrderByIdDesc(yourPhone);
                if (volunteer!=null) {
                    return "Đã Có Người Dùng Số Điện Thoại Bạn Nhập";
                }
            }
            if (yourEmail!=null &&
                    !yourEmail.equals("")) {
                volunteer = volunteerRepository.
                        findFirstByEmailOrderByIdDesc(yourEmail);
                if (volunteer != null) {
                    return "Đã Có Người Dùng Email Bạn Nhập";
                }
            }

            Date date = new Date();
            Account account = new Account();
//            account.setVolunteer(volunteer);
            account.setUsername(userName);
            account.setPassword(password);
            account.setCreated_at(date);
            account.setRoles("USER");
            account.setEnabled(1);
            account = accountRepository.save(account);

            volunteer = new Volunteer();

            volunteer.setName(yourName);
            volunteer.setPhone(yourPhone);
            volunteer.setEmail(yourEmail);
            volunteer.setCreated_at(date);
            volunteer.setAccount(account);
            volunteerRepository.save(volunteer);
//            volunteer = volunteerRepository.findFirstByPhoneOrderByIdDesc(yourPhone);


            return "";
        }
    }


    @Override
    public void deleteAccount(String username) {

    }

    @Override
    public long countVolunteer() {
        return accountRepository.countVolunteer();
    }

    @Autowired
    private DonateRepository donateRepository;
    @Override
    public List<Vote> getListYourDonate(Account account, int page, int size) {
        if (account!=null){
            return donateRepository.getListAccountDonate(account.getVolunteer(), PageRequest.of(page,size)).toList();
        }
        return null;
    }

    @Override
    public int amountPageYourDonate(String userName, int size) {
        Account account = accountRepository.findFirstByUsername(userName);
        if (account!=null){
            int count = donateRepository.getListAccountDonate(account.getVolunteer()).size();
            return (int) Math.ceil((double) count/size);
        }
        return 0;
    }

    @Autowired
    private VoteRepository voteRepository;
    @Override
    public long amountYouDonatedForVote(Account account, Vote vote) {
        if ((account!=null) && (vote!=null)){
            return donateRepository.amountAccountForVote(account.getVolunteer(),vote);
        }
        return 0;
    }
}
