package com.charity.cms.service;

import com.charity.cms.model.Account;
import com.charity.cms.model.Volunteer;
import com.charity.cms.model.Vote;

import java.util.List;

public interface AccountService {
    Account findById(Long id);
    Account findByUsername(String username);
    void update(Account originAccount, Account newAccount);
    String create(String userName,String password,String yourName,String yourEmail,String yourPhone);
    void deleteAccount(String username);

    long countVolunteer();

    List<Vote> getListYourDonate(Account account, int page, int size);
    int amountPageYourDonate(String userName, int size);
    long amountYouDonatedForVote(Account account, Vote vote);
}
