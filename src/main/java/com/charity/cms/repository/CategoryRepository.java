package com.charity.cms.repository;

import com.charity.cms.model.Category;
import com.charity.cms.model.Vote;
import org.springframework.data.repository.CrudRepository;

public interface CategoryRepository extends CrudRepository<Category, Long> {
    public Iterable<Category> findAllByVotes(Vote vote);
}
